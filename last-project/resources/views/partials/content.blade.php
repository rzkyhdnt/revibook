@extends('partials.master')

@section('content')
<section class="featured-places" id="blog">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading">
                        <span>Testimoni</span>
                        <h2>Judul buku untuk direview :</h2>
                    </div>
                </div> 
            </div> 
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="featured-item">
                        <div class="thumb">
                            <img src="{{asset('/images/harrypotter.jpg')}}" alt="">
                            <div class="overlay-content">
                                <ul>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                </ul>
                            </div>
                        </div>
                        <div class="down-content">
                            <h4>Harry Potter and The Deathly Hallows</h4>
                            <span>Category One</span>
                            <p>Harry Potter and the Deathly Hallows adalah film drama fantasi yang diadaptasi dari novel yang berjudul sama karya J. K. Rowling. Film ini dibagi menjadi dua bagian dan masing-masing dirilis pada tahun 2010 dan 2011. Film ini adalah film ketujuh dan terakhir dari seri film populer Harry Potter. Shooting film ini dimulai pada Februari 2009. Film ini dirilis dalam format 3D dan 2D serta layar IMAX.</p>
                            <div class="row">
                                <div class="col-md-6 first-button">
                                    <div class="text-button">
                                        <a href="#">Add to favorites</a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="text-button">
                                        <a href="#">Continue Reading</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="featured-item">
                        <div class="thumb">
                            <img src="{{asset('/images/laskarpelangi.jpg')}}" alt="">
                            <div class="overlay-content">
                                <ul>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                </ul>
                            </div>
                        </div>
                        <div class="down-content">
                            <h4>Laskar Pelangi</h4>
                            <span>Category Two</span>
                            <p>Novel berjudul Laskar Pelangi ini adalah novel pertama dari serangkaian tetralogi milik Andrea Hirata. Buku lanjutan Laskar Pelangi ini, berturut-turut adalah Sang Pemimpi, Endesor, serta Maryamah Karpov. Laskar Pelangi sendiri telah menjadi buku sastra terlaris sepanjang sejarah perbukuan di Indonesia. Novel apik ini telah diterbitkan di berbagai benua dalam berbagai bahasa.</p>
                            <div class="row">
                                <div class="col-md-6 first-button">
                                    <div class="text-button">
                                        <a href="#">Add to favorites</a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="text-button">
                                        <a href="#">Continue Reading</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="featured-item">
                        <div class="thumb">
                            <img src="{{asset('/images/davinci.jpg')}}" alt="">
                            <div class="overlay-content">
                                <ul>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                </ul>
                            </div>
                        </div>
                        <div class="down-content">
                            <h4>Da Vinci Code</h4>
                            <span>Category Three</span>
                            <p>The Da Vinci Code adalah sebuah novel detektif misteri karya Dan Brown. Novel ini menceritakan simbolog Robert Langdon dan kriptolog Sophie Neveu setelah suatu peristiwa pembunuhan di Museum Louvre di Paris, ketika mereka menjadi terlibat dalam pertarungan antara Biarawan Sion dan Opus Dei terkait kemungkinan bahwa apakah Yesus Kristus menikahi Maria Magdalena</p>
                            <div class="row">
                                <div class="col-md-6 first-button">
                                    <div class="text-button">
                                        <a href="#">Add to favorites</a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="text-button">
                                        <a href="#">Continue Reading</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-4 col-sm-6 col-xs-12 mt-5">
                    <div class="featured-item">
                        <div class="thumb">
                            <img src="{{asset('/images/bodoamat.jpg')}}" alt="">
                            <div class="overlay-content">
                                <ul>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                </ul>
                            </div>
                        </div>
                        <div class="down-content">
                            <h4>Sebuah Seni Untuk Bersikap Bodo Amat</h4>
                            <span>Category Four</span>
                            <p>Buku ini menceritakan tentang Harles Bokowski seorang pecandu alkohol senang bermain perempuan,pejudi kronis,kasar,kikir,dan tukang utang. Ia bercita-cita menjadi seorang penulis.karya Bukowski selalu di tolak oleh hampir setiap majalah,tetapi hal tersebut tidak membuat nya menyerah ia tetap menulis dan membuat puisi. Berpuluh tahun Bukowski hidup sebagai penyair dan kehidupan yang buruk.</p>
                            <div class="row">
                                <div class="col-md-6 first-button">
                                    <div class="text-button">
                                        <a href="#">Add to favorites</a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="text-button">
                                        <a href="#">Continue Reading</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12 mt-5">
                    <div class="featured-item">
                        <div class="thumb">
                            <img src="{{asset('/images/nick.jpg')}}" alt="">
                            <div class="overlay-content">
                                <ul>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                </ul>
                            </div>
                        </div>
                        <div class="down-content">
                            <h4>Life Without Limits</h4>
                            <span>Category One</span>
                            <p>Life Without Limits adalah buku inspiratif yang ditulis oleh orang biasa. Nick Vujicic, yang terlahir tanpa lengan dan tungkai, mengatasi cacat tubuhnya dengan menjalani kehidupan yang mandiri.Life Without Limit (2010) menceritakan tentang kehidupan seseorang yang memiliki keterbatasan fisik namun dapat menjalani kehidupannya secara luar biasa. Buku ini akan menginspirasi Anda untuk tidak menyerah dan tidak berputus asa dalam kondisi apapun.</p>
                            <div class="row">
                                <div class="col-md-6 first-button">
                                    <div class="text-button">
                                        <a href="#">Add to favorites</a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="text-button">
                                        <a href="#">Continue Reading</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12 mt-5">
                    <div class="featured-item">
                        <div class="thumb">
                            <img src="{{asset('/images/algoritma.jpg')}}" alt="">
                            <div class="overlay-content">
                                <ul>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                </ul>
                            </div>
                        </div>
                        <div class="down-content">
                            <h4>Algoritma dan Pemrograman</h4>
                            <span>Category One</span>
                            <p>Pada bab satu ini terdapat pengantar algoritma dan pemrograman. Pada bab ini terdapat 5 pokok pembahasan. Bab ini membahas pengetian program dan bahasa pemrograman, penerjemahan bahasa, penyelesaian masalah dengan program, tanya dan jawab, soal. Dengan tiga sub bahasan pada pokok bahasan ke tiga yang berisi tentang menganalisis masalah dan membuat algoritma, menuangkan algoritma bentuk program.</p>
                            <div class="row">
                                <div class="col-md-6 first-button">
                                    <div class="text-button">
                                        <a href="#">Add to favorites</a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="text-button">
                                        <a href="#">Continue Reading</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection